import java.util.Arrays;

public class Person {
    String name; //tên người
    int age; //tuổi
    String address;

    public Person(String name){
        this.name = name;
    }

    @Override
    public String toString(){
     return "Person [name = " + name + "]";
    }

}