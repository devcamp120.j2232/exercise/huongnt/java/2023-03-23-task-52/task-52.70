import java.util.Date;
import java.util.Locale;
import java.util.Arrays;
import java.text.NumberFormat;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.LocalDateTime;    

public class Order2 {
    int id;
    String customerName;
    Integer price;
    Date orderDate;
    Boolean confirm;
    String[] items;
    Person buyer;

    // khởi tạo đối tượng k tham số
    public Order2() {
    }

    // khởi tạo đối tượng 1 tham số
    public Order2(String customerName) {
        this.customerName = customerName;
    }

    // khởi tạo đối tượng 3 tham số

    public Order2(int id, String customerName, Integer price) {
        this.id = id;
        this.customerName = customerName;
        this.price = price;
    }

    // khởi tạo đối tượng all tham số

    public Order2(int id, String customerName, Integer price, Date orderDate, Boolean confirm, String[] items,
            Person buyer) {
        this.id = id;
        this.customerName = customerName;
        this.price = price;
        this.orderDate = orderDate;
        this.confirm = confirm;
        this.items = items;
        this.buyer = buyer;
    }

    @Override
    public String toString(){
        //định dạng tiêu chuẩn Việt Nam
        Locale.setDefault(new Locale("vi", "VN"));
        //định dạng cho ngày tháng
        String pattern = "dd-MMMM-yyyy HH:mm:ss.SSS";
        DateTimeFormatter defaultTimeFormatter = DateTimeFormatter.ofPattern(pattern);
        LocalDateTime now = LocalDateTime.now();  
        //định dạng cho giá tiền
        Locale usLocale = Locale.getDefault();
        NumberFormat usNumberFormat = NumberFormat.getCurrencyInstance(usLocale);
        //return chuỗi string
        return
            "Order [id =" + id
            + ", customerName =" + customerName
            //+ ", price =" + usNumberFormat.format(price)
            + ", price =" + price
            //+ ", orderDate =" + defaultTimeFormatter.format(orderDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime())
            + ", orderDate =" + now
            + ", confirm =" + confirm
            + ", items =" + Arrays.toString(items)
            + ", buyer =" + buyer;
    }

}
