import java.util.ArrayList;
import java.util.Date;

public class App {
    public static void main(String[] args) throws Exception {
        ArrayList<Order2> arrayList = new ArrayList<>();
        //khởi tạo order với các tham số khác nhau (Khởi tại 4 object Order)
        Order2 Order1 = new Order2();
        Order2 Order2 = new Order2("Lan");
        Order2 Order3 = new Order2 (3, "Long", 80000);
        Order2 Order4 = new Order2(4, "Nam", 75000, new Date(), false, new String[] {"hop mau", "tay", "giay mau"}, new Person("Huong"));
        //thêm objcect order vào danh sách
        arrayList.add(Order1);
        arrayList.add(Order2);
        arrayList.add(Order3);
        arrayList.add(Order4);

        //in ra màn hình
        for (Order2 order:arrayList){
            System.out.println(order.toString());
        }
    }
}
